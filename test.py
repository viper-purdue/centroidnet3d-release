import torch.utils.data
import torch.optim as optim
from tqdm import tqdm
import copy
from config import Config
from centroidnet import *
import skimage.external.tifffile as tif
import torch.nn as nn
import os
from utils.vis_tool import Visualizer, P_R_curve
import utils.array_tool as at
from utils.watershed3d import watershed3d
from utils.CC2 import CC2
import pandas as pd
from skimage.morphology import erosion, ball, dilation, cube
from utils.eval_f1 import eval_count, avg_dice, shift_centroids, eval_count_adaptive, eval_semantic_seg
from utils.mean_average_precision import MeanAveragePrecision
os.environ["CUDA_VISIBLE_DEVICES"]='1'

def create_data_loader(data_dir,ignore_vox_thresh, num_classes, crop, max_dist, repeat, sub, div):
    test_dir = os.path.join(data_dir, 'test_3D_tif')
    testing_set = centroidnet.NucleusDataset(test_dir, ignore_vox_thresh = ignore_vox_thresh, max_dist = max_dist, num_classes = num_classes,
                                                crop = crop, repeat = repeat, sub = sub, div = div, transform=False, mode='val')
    return testing_set


def create_centroidnet(num_channels, num_classes):
    model = centroidnet.CentroidNet(num_classes, num_channels)
    return model


def create_centroidnet_loss():
    if Config.loss == 'FocalTverskyLoss':
        loss = [centroidnet.CentroidLoss(), centroidnet.TverskyLoss(alpha=0.3, beta=0.7), centroidnet.FocalLoss()]
    else:
        loss = [centroidnet.CentroidLoss(), centroidnet.DiceLoss(), nn.BCELoss()]
    return loss

def validate(validation_loss, epoch, validation_set_loader, model, loss, validation_interval=10):
    if epoch % validation_interval == 0:
        with torch.no_grad():
            # Validate using validation data loader
            model.eval() # put in evaluation mode
            validation_loss = 0
            idx = 0
            for sample in validation_set_loader:
                inputs = sample['image']
                targets = sample['target']
                inputs = inputs.to(Config.dev)
                targets = targets.to(Config.dev)
                outputs = model(inputs)
                ls1 = 10*loss[0](outputs[:,0:3,:,:,:], targets[:,0:3,:,:,:])   # MSE Loss
                ls3 = 10*loss[2](outputs[:,3:5,:,:,:], targets[:,3:5,:,:,:])   # BCE Loss
                ls2 = loss[1](outputs[:,3:5,:,:,:], targets[:,3:5,:,:,:])   # Dice Loss
                ls = ls1 + ls2 + ls3
                validation_loss += ls.item()
                idx += 1
            model.train() # put back in training mode

            return validation_loss / idx
    else:
        return validation_loss


def save_model(filename, model):
    print(f"Save snapshot to: {os.path.abspath(filename)}")
    with open(filename, "wb") as f:
        torch.save(model.state_dict(), f)


def load_model(filename, model):
    print(f"Load snapshot from: {os.path.abspath(filename)}")
    with open(filename, "rb") as f:
        state_dict = torch.load(f)
    model.load_state_dict(state_dict)
    return model

def predict(data_set, model, loss, max_dist, binning, nm_size, centroid_threshold):
    print(f"Predicting {len(data_set)} files with loss {type(loss)}")
    with torch.no_grad():
        data_set.eval()
        model.eval()
        model.to(Config.dev)
        loss_value = 0
        idx = 0
        # inference only support batch size = 1
        set_loader = torch.utils.data.DataLoader(data_set, batch_size=1, shuffle=False, num_workers=1, drop_last=False)
        result_images = []
        result_centroids = []
        obj_codes = []
        for sample in tqdm(set_loader):
            inputs = sample['image']
            targets = sample['target']
            obj_codes.append(sample['obj_code'])
            inputs = inputs.to(Config.dev)
            targets = targets.to(Config.dev)
            outputs = model(inputs)
            ls1 = 10*loss[0](outputs[:,0:3,:,:,:], targets[:,0:3,:,:,:])   # MSE Loss
            ls3 = 10*loss[2](outputs[:,3:5,:,:,:], targets[:,3:5,:,:,:])   # BCE Loss
            ls2 = loss[1](outputs[:,3:5,:,:,:], targets[:,3:5,:,:,:])   # Dice Loss
            ls = ls1 + ls2 + ls3
            loss_value += ls.item()

            decoded = [centroidnet.decode(img, max_dist, binning, nm_size, centroid_threshold) for img in outputs.cpu().numpy()]

            # Add all numpy arrays to a list
            result_images.extend([{"inputs": i.cpu().numpy(),
                                   "targets": t.cpu().numpy(),
                                   "vectors": d[0],
                                   "votes": d[1],
                                   "class_ids": d[2],
                                   "class_probs": d[3],
                                   "centroids": d[4]} for i, t, o, d in zip(inputs, targets, outputs, decoded)])

            # Add image_id to centroid locations and add to list
            result_centroids.extend([np.stack(ctr for ctr in d[5]) for d in decoded])
            idx = idx + 1
    print("Aggregated loss is {:.5f}".format(loss_value / idx))
    return result_images, result_centroids, obj_codes


def output(folder, result_images, result_centroids, obj_codes):
    os.makedirs(folder, exist_ok=True)
    print(f"Created output folder {os.path.abspath(folder)}")
    
    for i, sample in enumerate(result_images):
        for name, arr in sample.items():
            if len(arr)!=0:
                if name == 'class_ids':
                    if Config.morphological<0:
                        morph = erosion(np.squeeze(arr), ball(abs(Config.morphological)))
                    elif Config.morphological>0:
                        morph = dilation(np.squeeze(arr), ball(abs(Config.morphological)))
                    else:
                        morph = np.squeeze(arr)
                    tif.imsave(os.path.join(folder, f"{str(i+1).zfill(2)}_{name}.tif"), np.transpose(morph, (2,0,1)), imagej=True)
                tif.imsave(os.path.join(folder, f"{str(i+1).zfill(2)}_{name}.tif"), np.transpose(arr, (3,0,1,2)), imagej=True)

    lines = ["image_nr centroid_y centroid_x centroid_z class_id probability \r\n"]
    with open(os.path.join(folder, "validation.txt"), "w") as f:
        for i, image in enumerate(result_centroids):
            for line in image:
                line_str = [str(i+1), *[str(elm) for elm in line]]
                lines.append(" ".join(line_str) + "\r\n")
        f.writelines(lines)

    # Instance segmentation using marker-controlled 3D watershed
    for i, sample in enumerate(result_images):
        instance_seg_vol = watershed3d(sample['class_ids'][0], sample['centroids'][0])
        tif.imsave(os.path.join(folder, f"{str(i+1).zfill(2)}_instance_seg.tif"), np.transpose(instance_seg_vol, (2,0,1)))
        # Color code the segmentation results
        cc_instance_seg_vol = CC2(instance_seg_vol)
        tif.imsave(os.path.join(folder, f"{str(i+1).zfill(2)}_instance_seg_CC2.tif"), np.transpose(cc_instance_seg_vol, (2,0,1,3)))

    if Config.mode == 'inference':
        sys.exit()

    # visualize centers & evaluate f1 score
    center_size = 1 if Config.data_name=='wsm' or Config.data_name=='tarek' else 2
    eval_results = []
    data_names = []
    count = []
    for i, (result_image, result_centroid) in enumerate(zip(result_images, result_centroids)):
        vis_volume = np.uint8(result_image['targets'][-1,:])
        coords = np.uint16(result_centroid[:,0:3])
        # coords = shift_centroids(coords, vis_volume)
        vis_volume = np.uint8(vis_volume[:,:,:,np.newaxis])
        vis_volume = np.repeat(vis_volume, 3, axis=3)
        vis_volume[vis_volume>0] = 64
        gt_centers = obj_codes[i].numpy()[0,:,0:3]
        print('number of predicted centers: '+str(len(coords)))
        count.append(len(coords))
        for pred_center in coords:
            vis_volume[(pred_center[0]-center_size):(pred_center[0]+center_size+1),(pred_center[1]-center_size):(pred_center[1]+center_size+1),(pred_center[2]-center_size):(pred_center[2]+center_size+1),0]=255
        
        for gt_center in gt_centers:
            vis_volume[(gt_center[0]-center_size):(gt_center[0]+center_size+1),(gt_center[1]-center_size):(gt_center[1]+center_size+1),(gt_center[2]-center_size):(gt_center[2]+center_size+1),1]=255
        tif.imsave(os.path.join(folder, f"{str(i+1).zfill(2)}_vis_center.tif"), np.transpose(vis_volume, (2,0,1,3)))
        precision, recall, f1 = eval_count(gt_centers, coords, dis_threshold=Config.dist_threshold[1])
        eval_results.append([precision, recall, f1])
        data_names.append('vol'+str(i+1).zfill(3))
    eval_results = np.array(eval_results)
    df = pd.DataFrame(zip(data_names, eval_results[:,0], eval_results[:,1], eval_results[:,2]), columns=['img_id','precision','recall','f1'])
    df.to_csv(os.path.join(folder, 'P_R_F1.csv'), index=False)
    count_df = pd.DataFrame(zip(data_names, np.array(count)), columns=['img_id', 'count'])
    count_df.to_csv(os.path.join(folder, 'count.csv'), index=False)
    print('precision:',np.round(np.average(df['precision']),4), ', recall:', np.round(np.average(df['recall']),4), ',f1:',np.round(np.average(df['f1']),4))

    # -----------evaluate mean average precision----------
    metric_fn = MeanAveragePrecision(num_classes=1)
    if 'update' in Config.checkpoints:       # evaluate mAP for last volume
        gt_centers = obj_codes[-1].numpy()[0,:,0:3]
        for i, (result_image, result_centroid) in enumerate(zip([result_images[-1]], [result_centroids[-1]])):
            coords = np.uint16(result_centroid[:,0:3])
            gt_ = np.concatenate([gt_centers, np.zeros((gt_centers.shape[0], 3))], axis=1)
            det_ = np.concatenate([coords, np.zeros((coords.shape[0], 2))], axis=1)
            metric_fn.add(det_, gt_)
    else:       # evaluate mAP for all volumes
        for i, (result_image, result_centroid) in enumerate(zip(result_images, result_centroids)):
            coords = np.uint16(result_centroid[:,0:3])
            gt_centers = obj_codes[i].numpy()[0,:,0:3]
            gt_ = np.concatenate([gt_centers, np.zeros((gt_centers.shape[0], 3))], axis=1)
            det_ = np.concatenate([coords, np.zeros((coords.shape[0], 2))], axis=1)
            metric_fn.add(det_, gt_)

    metric = metric_fn.value(dist_thresholds=np.arange(Config.dist_threshold[0], Config.dist_threshold[1]+1, 1), mpolicy='soft')
    APs = []
    Ts = []
    mAP = []
    for t in range(Config.dist_threshold[0], Config.dist_threshold[1]+1):
        Ts.append(np.round(t,4))
        APs.append(np.round(metric[t][0]['ap'],4))
        mAP.append(np.round(metric['mAP'],4))
    df_mAP = pd.DataFrame(np.concatenate([np.array([Ts]),np.array([APs]),np.array([mAP])]))
    df_mAP.index = ['distance thresholds', 'AP', 'mAP']
    df_mAP.to_csv(os.path.join(folder, 'mAP.csv'), header=False, float_format='%.4f')
    P_R_curve(metric, np.arange(Config.dist_threshold[0], Config.dist_threshold[1]+1, 1), folder)

    # -----------voxel based dice score, Type-I, Type-II, accuracy, IoU, sensitifity, specificity, F1----------
    seg_metrics = []
    for i, sample in enumerate(result_images):
        if Config.morphological<0:
            morph = erosion(np.squeeze(sample['class_ids']), ball(abs(Config.morphological)))
        elif Config.morphological>0:
            morph = dilation(np.squeeze(sample['class_ids']), ball(abs(Config.morphological)))
        else:
            morph = np.squeeze(sample['class_ids'])
        # TypeI, typeII, accuracy = avg_dice(morph, np.uint8(sample['targets'][4]))
        seg_metric = eval_semantic_seg(morph, np.uint8(sample['targets'][4]))
        seg_metric['vol_id'] = 'vol'+str(i+1).zfill(3)
        seg_metrics.append(seg_metric)
    
    df_seg_metrics = pd.DataFrame(seg_metrics)
    df_seg_metrics = df_seg_metrics.set_index('vol_id')
    df_seg_metrics.loc['mean'] = df_seg_metrics.mean()
    df_seg_metrics.to_csv(os.path.join(folder, 'seg_eval.csv'), float_format='%.4f')

    
def main():
    # Load datasets
    testing_set = create_data_loader(Config.test_dir, ignore_vox_thresh = Config.ignore_vox_thresh, 
                    num_classes = Config.num_classes, crop=Config.crop,max_dist=Config.max_dist, repeat=1, sub=Config.sub, div=Config.div)

    # Create loss function
    loss = create_centroidnet_loss()
    model = None

    # Load model
    model = create_centroidnet(num_channels=1, num_classes=testing_set.num_classes)
    model = nn.DataParallel(model)
    model = load_model(os.path.join("checkpoints", Config.checkpoints), model)

    # Predict
    result_images, result_centroids, obj_codes = predict(testing_set, model, loss, max_dist=Config.max_dist, binning=Config.binning, nm_size=Config.nm_size, centroid_threshold=Config.centroid_threshold)
    output(os.path.join("data", "testing_result_"+Config.data_name), result_images, result_centroids, obj_codes)


if __name__ == '__main__':
    main()
