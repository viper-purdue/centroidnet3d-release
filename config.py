class Config:
    data_name = 'D1'
    mode = 'test'  # mode: 'train', 'test', 'inference'
    if data_name == 'D1':
        data_dir = './data/D1_train_demo'    # training data dir
        test_dir = './data/D1_test'
        checkpoints = 'CentroidNet_D1_demo.pth'    # Load this checkpoint for testing
    elif data_name == 'D2':
        data_dir = ''
        test_dir = './data/D2_test'
        checkpoints = 'CentroidNet_D2_demo.pth'    # Load this checkpoint for testing

    dev = "cuda:0"
    env = 'centroidNet_' + data_name
    generate_in_fly = False  # set to True if data augmentation (random flip) is needed
    pre_train = False    # Load pre-trained model
    pre_train_path = '' # set this path if you want continue training
    # Loss function setting
    loss = 'FocalTverskyLoss'
    num_filters = 32
    block = 128
    # Random crops to take from the image during training and data augmentation.
    # This value should be large enough compared to your image size (in this case image size was (200 X 300) pixels.
    crop = [104]

    # This should reflect the mean and average in your dataset (or leave default for 8 bit images)
    sub = 127
    div = 256

    # Maximum allowed voting vector length. All votes are divided by this value during training.
    # This value should roughly be twice the max diameter of an object.
    max_dist_meta = {'D1':10, 'D2':24}
    max_dist = max_dist_meta[data_name]

    # Number of epochs to train. The best model with the best validation loss is kept automatically.
    # This value should typically be large. Check vectors.npy if the quality of vectors is ok.
    epochs = 200

    # Batch size for training. Choose a batch size which maximizes GPU memory usage.
    batch_size = 4
    # batch_size = 1

    # Learning rate. Usually this value is sufficient. We observed Explode when lr=0.001
    learn_rate = 0.001

    # Determines on what interval validation should occur.
    validation_interval = 5

    # Number of input channels of the image. Default is RGB.
    num_channels = 1

    # Number of classes. This is including the background, so this should be one more then in the training file.
    num_classes = 2

    # The amount of spatial binning to use (Values could be: 1, 2, 4, etc.)
    # Increase binning for increasing the robustness of the detection.
    # Decrease binning to increase spatial accuracy.
    binning_meta = {'D1':1,'D2':2}
    binning = binning_meta[data_name]

    # How far apart should two centroids minimally be. (values could be: 3, 7, 11, 15, 17, etc.)
    # Increase value to get less detection close together.
    # Decrease value to improve detection which are very close together.
    nm_size_meta = {'D1':3, 'D2':3}
    nm_size = nm_size_meta[data_name]

    # How many votes constitutes a centroid.
    # Increase value to increase precision and decrease recall (less detections)
    # Decrease value to increase recall and decrease precision (more detectons)
    # Determine a correct value by reviewing votes.npy
    centroid_threshold_meta = {'D1':30, 'D2':200}
    centroid_threshold = centroid_threshold_meta[data_name]

    # if the nuclei less than 125 voxels, ignore it.
    ignore_vox_thresh_meta = {'D1':70, 'D2':10}
    ignore_vox_thresh = ignore_vox_thresh_meta[data_name]


    # dist_threshold, for evaluate f1 score of centroid
    dist_threshold_meta = {'D1':(4, 8), 'D2':(6,12)} # adaptive f1 score
    dist_threshold = dist_threshold_meta[data_name]

    # For segmentation, set parameters for morphological erosion or dilation for different data
    # -1 means erode using structure size 1
    # 0 means no morphological operation
    morphological_meta = {'D1':0, 'D2':0}
    morphological = morphological_meta[data_name]