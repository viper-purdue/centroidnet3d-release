# Written by Liming Wu on 05/18/2020
import os
import numpy as np
from scipy.spatial import distance
import pandas as pd
from skimage import measure
from sklearn.metrics import confusion_matrix
import skimage.external.tifffile as tif
import utils.array_tool as at

def shift_centroids(coords, vis_volume):
    y, x, z = coords[:,0], coords[:,1], coords[:,2]
    y[y<=2] += 3
    x[x<=2] += 3
    z[z<=2] += 3
    y[y>=vis_volume.shape[0]-2] -= 3
    x[x>=vis_volume.shape[1]-2] -= 3
    z[z>=vis_volume.shape[2]-2] -= 3
    return np.array([y,x,z]).transpose()

def eval_count(gt_centers, det_centers, dis_threshold):
    # gt_centers: ground truth centers, numpy array, (N,2)
    # det_centers: detected nuclei centers, numpy array, (N,2)
    # this script estimate TP, FP, FN, and F1 score
    TP = 0
    FP = 0
    FN = 0
    for i, gt_center in enumerate(gt_centers):
        distances = []
        for j, det_center in enumerate(det_centers):
            distances.append(distance.euclidean(gt_center, det_center))
        # find closest center
        min_idx = np.argmin(distances)
        min_dist = np.min(distances)
        if min_dist <= dis_threshold:
            TP = TP + 1
            # delete the matched entry from det_centers
            det_centers = np.delete(det_centers, min_idx, axis=0)
            # if all detected centers are matched, but still some ground truth, then the remaining gt should be count as FN
            if len(det_centers) == 0:
                FN = FN + len(gt_centers) - (i + 1)
                break
        else:
            # no correspoinding detections
            FN = FN + 1
        
    FP = det_centers.shape[0]   # the remaining are FP
    print('TP:',TP,',FP:',FP,',FN:',FN)
    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    F1 = 2*precision*recall/(precision+recall)
    return round(precision,4), round(recall,4), round(F1,4)


def eval_count_adaptive(gt_centers, det_centers, dis_thresholds):
    precisions = []
    recalls = []
    f1s = []
    for dis_threshold in range(dis_thresholds[0], dis_thresholds[1]+1):
        p, r, f1 = eval_count(gt_centers, det_centers, dis_threshold)
        precisions.append(p)
        recalls.append(r)
        f1s.append(f1)
    return np.round(np.average(precisions),4), np.round(np.average(recalls),4), np.round(np.average(f1s),4)


def sum_batch_iou(pred_masks, gt_masks):
    pred_masks, gt_masks = at.tonumpy(pred_masks), at.tonumpy(gt_masks)
    B, C, Z, H, W = pred_masks.shape
    ious = []
    for i in range(B):
        pred_mask = pred_masks[i]
        gt_mask = gt_masks[i]

        union = np.sum(np.logical_or(pred_mask, gt_mask))
        intersection = np.sum(np.logical_and(pred_mask, gt_mask))
        if union==0:
            iou = 0
        else:
            iou = intersection/union
        ious.append(iou)
    batch_iou = np.sum(np.array(ious))

    return batch_iou

def avg_batch_iou(pred_masks, gt_masks):
    pred_masks, gt_masks = np.squeeze(at.tonumpy(pred_masks)), np.squeeze(at.tonumpy(gt_masks))
    ious = []
    for i in range(len(pred_masks)):
        pred_mask = pred_masks[i]
        gt_mask = gt_masks[i]
        # gt_mask = (gt_mask>=0.5).astype(np.float32)
        union = np.sum(np.logical_or(pred_mask, gt_mask))
        intersection = np.sum(np.logical_and(pred_mask, gt_mask))
        if union==0:
            iou = 0
        else:
            iou = intersection/union
        ious.append(iou)

    return np.array(ious)

def avg_dice(pred_masks, gt_masks):
    pred_masks, gt_masks = np.squeeze(at.tonumpy(pred_masks)), np.squeeze(at.tonumpy(gt_masks))
    tn, fp, fn, tp = confusion_matrix(gt_masks.flatten(), pred_masks.flatten()).ravel()
    print(tp+fn, tn+fp, pred_masks.shape)
    type_I = fp/(pred_masks.size)
    type_II = fn/(pred_masks.size)
    # dice = 2*tp/(2*tp+fp+fn)
    # accuracy = 2*sensitivity*specificity/(sensitivity+specificity)
    accuracy = (tp+tn)/(pred_masks.size)

    return np.round(type_I,4), np.round(type_II,4), np.round(accuracy,4)

def eval_semantic_seg(pred_masks, gt_masks):
    pred_masks, gt_masks = np.squeeze(at.tonumpy(pred_masks)), np.squeeze(at.tonumpy(gt_masks))
    tn, fp, fn, tp = confusion_matrix(gt_masks.flatten(), pred_masks.flatten()).ravel()
    print(tp+fn, tn+fp, pred_masks.shape)
    eval_results = {}
    eval_results["type_I"] = fp/(pred_masks.size)
    eval_results["type_II"] = fn/(pred_masks.size)
    eval_results["accuracy"] = (tp+tn)/(pred_masks.size)
    eval_results["sensitivity"] = tp/(tp+fn)
    eval_results["specificity"] = tn/(tn+fp)
    eval_results["dice"] = 2*tp/(2*tp+fp+fn)    # dice is f1
    eval_results["iou"] = tp/(tp+fp+fn)

    return eval_results

# There are some problem with it, 1. too slow, 2. results are not same as the Matlab version
def eval_instance_seg(gt_vol, pred_vol, iou_threshold=0.1):
    # gt_vol: ground truth label, each nucleus is marked by different intensity
    # pred_vol: predicted label, each nucleus is marked by different intensity
    # iou_threshold: the IoU threshold for two nuclei to be 'matched'
    TP = 0
    FP = 0
    FN = 0
    while np.count_nonzero(gt_vol)!=0 and np.count_nonzero(pred_vol)!=0:
        cc_gt = list(np.unique(gt_vol)[1:])
        cc_pred = list(np.unique(pred_vol)[1:])
        for i in cc_gt:
            # for each nucleus in gt_vol
            
            coords_gt_nucleus = np.where(gt_vol == i)
            coords_gt_nucleus = np.array([coords_gt_nucleus[0],coords_gt_nucleus[1],coords_gt_nucleus[2]]).transpose()
            coords_gt_nucleus_set = {tuple(i) for i in coords_gt_nucleus}
            ious = []
            max_coords = []
            for j in cc_pred:
                # for each nucleus in pred_vol
                coords_pred_nucleus = np.where(pred_vol==j)
                max_coords.append(coords_pred_nucleus)
                coords_pred_nucleus = np.array([coords_pred_nucleus[0],coords_pred_nucleus[1],coords_pred_nucleus[2]]).transpose()
                coords_pred_nucleus_set = {tuple(i) for i in coords_pred_nucleus}

                # compute IoU
                union = len(coords_gt_nucleus_set.union(coords_pred_nucleus_set))
                intersection = len(coords_gt_nucleus_set.intersection(coords_pred_nucleus_set))
                if union==0:
                    iou = 0
                else:
                    iou = intersection/union
                ious.append(iou)
                # if iou >= iou_threshold:
                #     # two nuclei are matched, TP is counted
                #     TP = TP + 1
                #     # remove the matched nuclei
                #     gt_vol[coords_gt_nucleus] = 0
                #     pred_vol[coords_pred_nucleus] = 0
            if np.max(ious)>=iou_threshold:
                TP = TP + 1
                gt_vol[coords_gt_nucleus[:,0],coords_gt_nucleus[:,1],coords_gt_nucleus[:,2]] = 0
                # pred_vol[coords_pred_nucleus] = 0

                pred_vol[max_coords[np.argmax(ious)]] = 0
            else:
                FN = FN + 1

                gt_vol[coords_gt_nucleus[:,0],coords_gt_nucleus[:,1],coords_gt_nucleus[:,2]] = 0
            break

    FP = FP + len(np.unique(pred_vol)[1:])  # the remaining (unmatched) nuclei in pred_vol is the FP
    FN = FN + len(np.unique(gt_vol)[1:])
    print('TP:',TP,'FP:',FP,'FN:',FN)
    precision = TP/(TP+FP)
    recall = TP/(TP+FN)
    F1 = 2*precision*recall/(precision+recall)
    return precision, recall, F1


if __name__ == '__main__':
    # pred_vol = tif.imread('/data/wu1114/p219/Documents/centroidNet3D/data/testing_result_wsm/0_instance_seg.tif')
    pred_vol = tif.imread('/data/wu1114/p219/Documents/centroidNet3D/deepsynth_output.tif')
    pred_vol = measure.label(pred_vol)
    pred_vol = np.transpose(pred_vol, (1,2,0))
    gt_vol = tif.imread('/data/wu1114/p219/Documents/dataset/wsm_microscopy/gt_tif/vol001.tif')
    a = np.zeros((128,128,64,3),dtype=np.uint8)
    a[np.where(pred_vol>0)[0],np.where(pred_vol>0)[1],np.where(pred_vol>0)[2],0]=255
    a[np.where(gt_vol>0)[0],np.where(gt_vol>0)[1],np.where(gt_vol>0)[2],1]=255
    tif.imsave('vis.tif', np.transpose(a, (2,0,1,3)))
    precision, recall, F1 = eval_instance_seg(gt_vol.copy(), pred_vol.copy())
    print('precision:',precision, 'recall:',recall, 'f1:',F1)
