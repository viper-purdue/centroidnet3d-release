import numpy as np
import matplotlib.pyplot as plt
from scipy import ndimage as ndi

from skimage.segmentation import watershed
from skimage.feature import peak_local_max
import skimage.external.tifffile as tif
import skimage.io as io
from skimage import measure
from scipy.ndimage.measurements import label
from skimage.morphology import erosion, dilation, ball

def shift_centroids(centroids_vol):
    y,x,z = np.where(centroids_vol==255)
    y[y<=3] += 6
    x[x<=3] += 6
    z[z<=3] += 6
    y[y>=centroids_vol.shape[0]-3] -= 3
    x[x>=centroids_vol.shape[1]-3] -= 3
    z[z>=centroids_vol.shape[2]-3] -= 3
    new_vol = np.zeros(centroids_vol.shape, centroids_vol.dtype)
    new_vol[y,x,z] = 255
    return new_vol
    

# Use the voting results as the markers
def watershed3d(volume, centroids):
    # volume = condition_erosion(volume)
    # volume = erosion(volume, ball(1))
    distance = ndi.distance_transform_edt(volume)
    
    local_maxi = shift_centroids(centroids)
    markers = ndi.label(local_maxi)[0]
    # labels = watershed(-distance, markers, mask=volume, watershed_line=True, compactness=0.01)
    labels = watershed(-distance, markers, mask=volume, watershed_line=True, compactness=0.03)
    # labels = dilation(labels, ball(3))
    labels = np.uint16(labels)
    return labels

def get_coarse_structure(size):
    coarse_z = ball(size)
    coarse_z[:,:,0] = 0
    coarse_z[:,:,-1] = 0

    coarse_y = ball(size)
    coarse_y[:,0,:] = 0
    coarse_y[:,-1,:] = 0

    coarse_x = ball(size)
    coarse_x[0,:,:] = 0
    coarse_x[-1,:,:] = 0

    return [coarse_x, coarse_y, coarse_z]

def condition_erosion(volume):
    # volume: binary volume
    # T1: nuclei voxels threshold for condition erosion with coarse erosion
    # T2: nuclei voxels threshold for condition erosion with fine erosion
    
    # Define fine structure
    fine_structure = ball(0)
    # Define coarse structure
    # coarse_structure = get_coarse_structure(1)
    coarse_structure = ball(0)
    # Determine T1 and T2 base on the percentile of the nuclei size
    cc = measure.label(volume,connectivity = 1)
    props = measure.regionprops(cc)
    areas = []
    for ii in range(1,np.amax(cc)):
        areas.append(props[ii].area)
    areas.sort()
    plt.hist(areas, bins='auto')
    plt.savefig('area_distribution.png')
    T1 = np.percentile(areas, 90)
    T2 = np.percentile(areas, 90)
    # Erode on volumes with coarse structure until the size of objects is smaller than T1
    while(True):
        cc = measure.label(volume,connectivity = 1)
        props = measure.regionprops(cc)
        erosion_nuclei_vol = np.zeros(volume.shape, dtype=volume.dtype)
        count_big = 0
        for ii in range(0,np.amax(cc)):
            if props[ii].area > T1:
                count_big += 1 
                temp_vol = np.zeros(volume.shape, dtype=volume.dtype)
                temp_vol[props[ii].coords[:,0],props[ii].coords[:,1],props[ii].coords[:,2]] = 1
                bbox = props[ii].bbox
                direction = np.argmax([bbox[3]-bbox[0], bbox[4]-bbox[1], bbox[5]-bbox[2]])
                # temp_vol = erosion(temp_vol, coarse_structure[direction])
                temp_vol = erosion(temp_vol, coarse_structure)
                # temp_vol = erosion(temp_vol, fine_structure)
                erosion_nuclei_vol = erosion_nuclei_vol + temp_vol
            else:
                erosion_nuclei_vol[props[ii].coords[:,0],props[ii].coords[:,1],props[ii].coords[:,2]] = 1
        
        volume = erosion_nuclei_vol
        if count_big == 0:
            break

    # Erode on volumes with fine structure until the size of objects is smaller than T2
    while(True):
        cc = measure.label(volume,connectivity = 1)
        props = measure.regionprops(cc)
        erosion_nuclei_vol = np.zeros(volume.shape, dtype=volume.dtype)
        count_big = 0
        for ii in range(0,np.amax(cc)):
            if props[ii].area > T2:
                count_big += 1 
                temp_vol = np.zeros(volume.shape, dtype=volume.dtype)
                temp_vol[props[ii].coords[:,0],props[ii].coords[:,1],props[ii].coords[:,2]] = 1
                bbox = props[ii].bbox
                direction = np.argmax([bbox[3]-bbox[0], bbox[4]-bbox[1], bbox[5]-bbox[2]])
                temp_vol = erosion(temp_vol, fine_structure)
                erosion_nuclei_vol = erosion_nuclei_vol + temp_vol
            else:
                erosion_nuclei_vol[props[ii].coords[:,0],props[ii].coords[:,1],props[ii].coords[:,2]] = 1
        volume = erosion_nuclei_vol
        if count_big == 0:
            break
    return volume

if __name__ == '__main__':
    path = '/data/wu1114/p219/Documents/centroidNet3D/data/testing_result_immu/0_class_ids.tif'
    volume = tif.imread(path)
    print(volume.shape, volume.dtype, np.max(volume))
    volume = condition_erosion(volume)
    print(volume.shape, volume.dtype)
