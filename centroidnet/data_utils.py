import os
import random
import pandas as pd
import numpy as np
from tqdm import tqdm
from shutil import copyfile
import matplotlib.pyplot as plt
from skimage.io import imsave, imread
from PIL import Image
from skimage import transform

def random_flip(img, mask, y_random=False, x_random=False,z_random=False,
                return_param=False, copy=True):
    """
    Randomly flip an image in vertical or horizontal direction.
    :param img (numpy.ndarray): An array that gets flipped.
        This is in CHW format.
    :param y_random (bool): Randomly flip in vertical direction.
    :param x_random (bool): Randomly flip in horizontal direction.
    :param return_param (bool): Returns information of flip.
    :param copy (bool): If False, a view of :obj:`img` will be returned.
    :return (numpy.ndarray or (numpy.ndarray, dict)):
        If :`return_param = False`,
        returns an array :obj:`out_img` that is the result of flipping.
        If :obj:`return_param = True`,
        returns a tuple whose elements are :obj:`out_img, param`.
        :obj:`param` is a dictionary of intermediate parameters whose
        contents are listed below with key, value-type and the description
        of the value.
        * **y_flip** (*bool*): Whether the image was flipped in the\
            vertical direction or not.
        * **x_flip** (*bool*): Whether the image was flipped in the\
            horizontal direction or not.
    """
    y_flip, x_flip, z_flip= False, False, False
    if y_random:
        y_flip = random.choice([True, False])
    if x_random:
        x_flip = random.choice([True, False])
    if z_random:
        z_flip = random.choice([True, False])

    if y_flip:
        img = img[:, ::-1, :]
        mask = mask[:, ::-1, :]
    if x_flip:
        img = img[:, :, ::-1]
        mask = mask[:, :, ::-1]
    if z_flip:
        img = img[:,:,::-1]
        mask = mask[:,:,::-1]

    if copy:
        img = img.copy()
        mask = mask.copy()

    if return_param:
        return img, mask, {'y_flip': y_flip, 'x_flip': x_flip, 'z_flip':z_flip}
    else:
        return img, mask
