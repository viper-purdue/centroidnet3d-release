import torch
import torch.nn as nn
import torch.autograd
import torch.nn.modules.loss
import torch.nn.functional as F
from centroidnet.backbones import UNet
from centroidnet.backbones.deepsynth import encoder_decoder, unet_resnet, encoder_decoder_noise, encoder_decoder_att,encoder_decoder_att_noise,AttR2UNet,ResAttUNet
from centroidnet.backbones.Rtt_UNet import R2AttU_Net,AttU_Net
import numpy as np
import sys
from skimage import measure
from typing import List
from skimage.draw import ellipse
from skimage.feature import peak_local_max
from skimage.transform import rescale
from config import Config

class CentroidNet(torch.nn.Module):
    def __init__(self, num_classes, num_channels):
        torch.nn.Module.__init__(self)
        self.backbone = encoder_decoder(in_dim=1, out_dim=num_classes+3, num_filter=Config.num_filters)
        # self.backbone = encoder_decoder_noise(in_dim=1, out_dim=num_classes+3, num_filter=Config.num_filters)
        # self.backbone = encoder_decoder_att(in_dim=1, out_dim=num_classes+3, num_filter=Config.num_filters)
        # self.backbone = encoder_decoder_att_noise(in_dim=1, out_dim=num_classes+3, num_filter=Config.num_filters)
        # self.backbone = AttR2UNet(in_dim=1, out_dim=num_classes+3, num_filter=Config.num_filters)
        # self.backbone = ResAttUNet(in_dim=1, out_dim=num_classes+3, num_filter=Config.num_filters)
        self.num_classes = num_classes
        self.num_channels = num_channels

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        return self.backbone(x)

    def __str__(self):
        return f"CentroidNet: {self.backbone}"


class CentroidLoss(torch.nn.Module):
    def __init__(self):
        nn.Module.__init__(self)
        self.loss = 0

    def forward(self, result, target):
        mse_loss = nn.MSELoss(size_average=True, reduction='mean')
        loss = mse_loss(result, target)
        self.loss = loss.item()    # scale the loss
        return loss

    def __str__(self):
        return f"{self.loss}"

class DiceLoss(nn.Module):
    def __init__(self, weight=None, size_average=True):
        super(DiceLoss, self).__init__()

    def forward(self, inputs, targets, smooth=1):
        
        #comment out if your model contains a sigmoid or equivalent activation layer
        # inputs = F.sigmoid(inputs)       
        
        #flatten label and prediction tensors
        inputs = inputs.reshape(-1)
        targets = targets.reshape(-1)
        
        intersection = (inputs * targets).sum()                            
        dice_loss = 1 - (2.*intersection + smooth)/(inputs.sum() + targets.sum() + smooth)  
        return dice_loss

# class FocalLoss(nn.Module):
#     def __init__(self, weighht=None, size_average=True):
#         super(FocalLoss, self).__init__()

#     def forward(self, inputs, targets, alpha=0.8, gamma=2, smooth=1):
#         inputs = inputs.reshape(-1)
#         targets = targets.reshape(-1)
#         # first compute binary cross-entropy 
#         BCE = F.binary_cross_entropy(inputs, targets, reduction='mean')
#         BCE_EXP = torch.exp(-BCE)
#         focal_loss = alpha * (1-BCE_EXP)**gamma * BCE
#         return focal_loss

class FocalLoss(nn.Module):
    def __init__(self, weighht=None, size_average=True, alpha=0.8, gamma=2):
        super(FocalLoss, self).__init__()
        self.alpha = alpha
        self.gamma = gamma

    def forward(self, inputs, targets, smooth=1):
        inputs = inputs.reshape(-1)
        targets = targets.reshape(-1)
        # first compute binary cross-entropy 
        BCE = F.binary_cross_entropy(inputs, targets, reduction='mean')
        BCE_EXP = torch.exp(-BCE)
        focal_loss = self.alpha * (1-BCE_EXP)**self.gamma * BCE
        return focal_loss

class TverskyLoss(nn.Module):
    def __init__(self, alpha=0.3, beta=0.7, smooth=1, weight=None, size_average=True):
        super(TverskyLoss, self).__init__()
        self.alpha = alpha
        self.beta = beta
        self.smooth = smooth

    def forward(self, inputs, targets):
                
        #flatten label and prediction tensors
        inputs = inputs.reshape(-1)
        targets = targets.reshape(-1)
        
        #True Positives, False Positives & False Negatives
        TP = (inputs * targets).sum()    
        FP = ((1-targets) * inputs).sum()
        FN = (targets * (1-inputs)).sum()
       
        Tversky = (TP + self.smooth) / (TP + self.alpha*FP + self.beta*FN + self.smooth)  
        
        return 1 - Tversky


# class BCEDiceLoss(nn.Module):
#     def __init__(self, lambda_1=1, lambda_2=1):
#         super(BCEDiceLoss, self).__init__()
#         self.bce_loss = nn.BCELoss().cuda()
#         self.lambda_1 = lambda_1
#         self.lambda_2 = lambda_2

#     def forward(self, preds, target):
#         """
#         input is a torch variable of size BatchxnclassesxHxW representing log probabilities for each class
#         target is a 1-hot representation of the groundtruth, shoud have same size as the input
#         """
#         # 1. Measure the Dice Loss
#         preds = preds.squeeze(1)
#         target = target.squeeze(1)
#         # print input.size()
#         assert preds.size() == target.size(), "Input sizes must be equal."
#         assert preds.dim() == 4, "Input must be a 4D Tensor."
#         uniques=np.unique(target.cpu().data[0].numpy())
#         assert set(list(uniques))<=set([0,1]), "target must only contain zeros and ones"
#         # probs=F.softmax(input)
#         # print(input)
#         # print(probs)
#         probs = preds
#         num=probs*target#b,c,h,w--p*g
#         num=torch.sum(num,dim=3)#b,c,h
#         num=torch.sum(num,dim=2)
#         num=torch.sum(num,dim=1)

#         den1=probs*probs#--p^2
#         den1=torch.sum(den1,dim=3)#b,c,h
#         den1=torch.sum(den1,dim=2)
#         den1=torch.sum(den1,dim=1)
        
#         den2=target*target#--g^2
#         den2=torch.sum(den2,dim=3)#b,c,h
#         den2=torch.sum(den2,dim=2)#b,c
#         den2=torch.sum(den2,dim=1)
#         dice=2*(num/(den1+den2))
#         # print dice.size()
#         dice_eso=dice#we ignore bg dice val, and take the fg
#         dice_total=-1*torch.sum(dice_eso)/dice_eso.size(0)#divide by batch_sz

#         # 2. Measure the BCE Loss
#         total_bce_loss = self.bce_loss(preds, target)

#         # 3. total loss = lambda_1 * bec loss + lambda_2 * dice loss
#         total_loss = self.lambda_1 * total_bce_loss + self.lambda_2 * dice_total
#         return total_loss

#     def __str__(self):
#         return f"{self.loss}"
        

def encode(coords, vol_gt, image_height: int, image_width: int, image_depth: int, max_dist: int, num_classes: int):
    y_coords, x_coords, z_coords, _, _, _, _, _, _, _, = np.transpose(coords)

    #Encode vectors
    target_vectors = calc_vector_distance(y_coords, x_coords, z_coords, image_height, image_width, image_depth, max_dist)
    if not max_dist is None:
        target_vectors /= max_dist
    target_vectors = np.transpose(target_vectors, [3, 0, 1, 2])

    #Encode logits (bounding box is drawn as ellipses)
    target_logits = np.zeros((num_classes, image_height, image_width, image_depth))
    target_logits[0] = 1
    target_logits[0][vol_gt>0] = 0
    target_logits[1][vol_gt>0] = 1

    target = np.concatenate((target_vectors, target_logits))
    return target


def decode(input : np.ndarray, max_dist: int, binning: int, nm_size: int, centroid_threshold: int):
    _, image_height, image_width, image_depth = input.shape
    centroid_vectors = input[0:3] * max_dist
    logits = input[3:]

    #Calculate class ids and class probabilities
    class_ids = np.expand_dims(np.argmax(logits, axis=0), axis=0).astype(np.int16)
    sum_logits = np.expand_dims(np.sum(logits, axis=0), axis=0)
    class_probs = np.expand_dims(np.max((logits / sum_logits), axis=0), axis=0)
    class_probs = np.clip(class_probs, 0, 1)

    # Calculate the centroid images
    votes = calc_vote_image(centroid_vectors, binning)
    votes_nm = peak_local_max(votes[0], min_distance=nm_size, threshold_abs=centroid_threshold, indices=False, exclude_border=False,num_peaks_per_label=1)
    votes_nm = np.expand_dims(votes_nm, axis=0)
    votes_nm = np.uint8(votes_nm)*255
    # Calculate list of centroid statistics
    coords = np.transpose(np.where(votes_nm[0] > 0))
    centroids = [[y * binning, x * binning, z * binning, class_ids[0, y * binning, x * binning, z * binning] - 1, class_probs[0, y * binning, x * binning, z * binning]] for (y, x, z) in coords]
    # convert the votes and votes_nm to the same dimension as input image
    votes_nm = np.zeros((1, image_height, image_width, image_depth), np.uint8)
    for (y,x,z) in coords:
        votes_nm[0, y*binning, x*binning, z*binning] = 255
    votes_rescale = rescale(votes[0], scale=binning, order=0)   # interpolate with nearest neighbor
    votes_rescale = votes_rescale[np.newaxis,:]

    # length of centroids cannot represent the number of centroids because some peaks might have multiple pixels, if they are adjacent peaks. But it does not affect marker based watershed.
    return centroid_vectors, votes_rescale, class_ids, class_probs, votes_nm, centroids


def calc_vector_distance(y_coords: List[int], x_coords: List[int], z_coords: List[int], image_height: int, image_width: int, image_depth: int, max_dist) -> np.ndarray:
    assert (len(y_coords) == len(x_coords)), "list of coordinates should be the same"
    assert (len(y_coords) > 0), "No centroids in source image"

    # Prepare datastructures
    shape = [image_height, image_width, image_depth]
    image_coords = np.indices(shape)
    image_coords_planar = np.transpose(image_coords, [1, 2, 3, 0])

    dist_cube = np.empty([len(y_coords), image_height, image_width, image_depth])
    vec_cube = np.empty([len(y_coords), image_height, image_width, image_depth, 3])

    # Create multichannel image with distances and vectors
    for (i, (y, x, z)) in enumerate(zip(y_coords, x_coords, z_coords)):
        vec = np.array([y, x, z]) - image_coords_planar
        vec_cube[i] = vec
        dist = vec ** 2
        dist = np.sum(dist, axis=3)
        dist = np.sqrt(dist)
        dist_cube[i] = dist

    # Get the smallest centroid distance index
    dist_ctr_labels = np.argmin(dist_cube, axis=0)

    # Get the smallest distance vectors [h, w, yx]
    vec_ctr = vec_cube[dist_ctr_labels, image_coords[0], image_coords[1], image_coords[2]]

    # Clip vectors
    if not max_dist is None:
        active = np.sqrt(np.sum(vec_ctr ** 2, axis=3)) > max_dist
        vec_ctr[active, :] = 0

    return vec_ctr


def calc_vote_image(centroid_vectors: np.array, f) -> np.ndarray:
    channels, height, width, depth = centroid_vectors.shape

    size = np.array(np.array((height, width, depth), dtype=np.float) * ((1/f), (1/f), (1/f)), dtype=np.int)
    indices = np.indices((height, width, depth), dtype=centroid_vectors.dtype)

    # Calculate absolute vectors
    vectors = ((centroid_vectors + indices) * (1/f)).astype(np.int)
    nimage = np.zeros((size[0], size[1], size[2]))

    # Clip pixels
    logic = np.logical_and(np.logical_and(np.logical_and(vectors[0] >= 0, vectors[1] >= 0), vectors[2] >= 0), 
                            np.logical_and(np.logical_and(vectors[0] < size[0], vectors[1] < size[1]), vectors[2] < size[2]))
    coords = vectors[:, logic]

    # Accumulate
    np.add.at(nimage, (coords[0], coords[1], coords[2]), 1)
    return np.expand_dims(nimage, axis=0).astype(np.float32)
