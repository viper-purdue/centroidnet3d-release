import os
from torch.utils.data import Dataset
import torch
import numpy as np
import cv2
import sys
import random
from config import Config
import centroidnet
from skimage.filters import gaussian
import skimage.external.tifffile as tif
from .data_utils import random_flip

class CentroidNetDataset(Dataset):
    """
    CentroidNetDataset Dataset
        Load centroids from txt file and apply vector aware data augmentation

    Arguments:
        filename: filename of the input data format: (image_file_name,xmin, xmax, ymin, ymax,class_id\lf)
        crop (h, w): Random crop size
        transpose ((dim2, dim3)): List of random transposes to choose from
        stride ((dim2, dim3)): List of random strides to choose from
    """
    def convert_path(self, path):
        if os.path.isabs(path):
            return path
        else:
            return os.path.join(self.data_path, path)

    def load_and_convert_data(self, filename, max_dist=None):
        with open(filename) as f:
            lines = f.readlines()
        lines = [x.strip().split(",") for x in lines]
        self.count = len(lines)
        img_ctrs = {}
        for line in lines:
            fn, xmin, xmax, ymin, ymax, id = line
            xmin, xmax, ymin, ymax, id = int(xmin), int(xmax), int(ymin), int(ymax), int(id)
            x, y = (xmin + xmax) // 2, (ymin + ymax) // 2
            if not fn in img_ctrs:
                img_ctrs[fn] = list()

            self.num_classes = id+2 if id+2 > self.num_classes else self.num_classes #including background class
            img_ctrs[fn].append(np.array([y, x, ymin, ymax, xmin, xmax, id], dtype=int))
        for key in img_ctrs.keys():
            img_ctrs[key] = np.stack(img_ctrs[key])
            fn = self.convert_path(key)
            img = cv2.imread(fn)

            if img is None:
                raise Exception("Could not read {}".format(fn))

            crop = min(img.shape[0], img.shape[1], self.crop[0], self.crop[1])
            if crop != self.crop[0]:
                print(f"Warning: random crop adjusted to {[crop, crop]}")
                self.set_crop([crop, crop])

            target = centroidnet.encode(img_ctrs[key], img.shape[0], img.shape[1], max_dist, self.num_classes)
            img = (np.transpose(img, [2, 0, 1]).astype(np.float32) - self.sub) / self.div
            target = target.astype(np.float32)
            self.images.append(img)
            self.targets.append(target)

    def __init__(self, filename: str, crop=(256, 256), max_dist=100, repeat=1, sub=127, div=256, transpose=np.array([[0, 1], [1, 0]]), stride=np.array([[1, 1], [-1, -1], [-1, 1], [1, -1]]), data_path=None):
        self.count = 0
        if data_path is None:
            self.data_path = os.path.dirname(filename)
        else:
            self.data_path = data_path

        self.filename = filename
        self.images = list()
        self.targets = list()

        self.sub = sub
        self.div = div
        self.repeat = repeat
        self.set_crop(crop)
        self.set_repeat(repeat)
        self.set_transpose(transpose)
        self.set_stride(stride)
        self.num_classes = 0
        self.train()
        self.load_and_convert_data(filename, max_dist=max_dist)

    def eval(self):
        self.train_mode = False

    def train(self):
        self.train_mode = True

    def set_repeat(self, repeat):
        if repeat < 0:
            self.repeat = 1
        self.repeat = repeat

    def set_crop(self, crop):
        self.crop = crop

    def set_transpose(self, transpose):
        if np.all(transpose == np.array([[0, 1]])):
            self.transpose = None
        else:
            self.transpose = transpose

    def set_stride(self, stride):
        if np.all(stride == np.array([[1, 1]])):
            self.stride = None
        else:
            self.stride = stride

    def adjust_vectors(self, img, transpose, stride):
        if not transpose is None:
            img2 = img.copy()
            img[0] = img2[transpose[0]]
            img[1] = img2[transpose[1]]
        if not stride is None:
            img2 = img.copy()
            img[0] = img2[0] * stride[0]
            img[1] = img2[1] * stride[1]
        return img

    def adjust_image(self, img, transpose, slice, crop, stride):
        if not transpose is None:
            img = np.transpose(img, (0, transpose[0] + 1, transpose[1] + 1))
        if not slice is None:
            img = img[:, slice[0]:slice[0] + crop[0], slice[1]:slice[1] + crop[1]]
        if not stride is None:
            img = img[:, ::stride[0], ::stride[1]]
        return img

    def get_target(self, img: np.array, transpose, slice, crop, stride):
        img[0:2] = self.adjust_vectors(img[0:2], transpose, stride)
        img = self.adjust_image(img, transpose, slice, crop, stride)
        return img

    def get_input(self, img: np.array, transpose, slice, crop, stride):
        img = self.adjust_image(img, transpose, slice, crop, stride)
        return img

    def __getitem__(self, index):
        index = index // self.repeat
        input, target = self.images[index], self.targets[index]

        if self.stride is None and self.transpose is None and self.crop is None or not self.train_mode:
            return input.astype(np.float32), target.astype(np.float32)

        if not self.transpose is None:
            transpose = random.choice(self.transpose)
        else:
            transpose = None

        if not self.stride is None:
            stride = random.choice(self.stride)
        else:
            stride = None

        if not self.crop is None:
            min = np.array([0, 0])
            if not transpose is None:
                max = np.array([input.shape[transpose[0] + 1], input.shape[transpose[1] + 1]], dtype=int) - self.crop
            else:
                max = np.array([input.shape[1] - self.crop[0], input.shape[2] - self.crop[1]])
            slice = [random.randint(mn, mx) for mn, mx in zip(min, max)]
        else:
            slice = None

        input = self.get_input(input, transpose, slice, self.crop, stride).astype(np.float32)
        target = self.get_target(target, transpose, slice, self.crop, stride).astype(np.float32)

        return input, target

    def __len__(self):
        return len(self.images) * self.repeat

# Load Immu training dataset
class NucleusDataset(Dataset):
    def __init__(self, root_dir, ignore_vox_thresh, max_dist, num_classes,
                                                crop, repeat, sub, div, transform=False, mode='train'):
        """
        Args:
        :param root_dir (string): Directory with all the images
        :param img_id (list): lists of image id
        :param train: if equals true, then read training set, so the output is image, mask and imgId
                      if equals false, then read testing set, so the output is image and imgId
        :param transform (callable, optional): Optional transform to be applied on a sample
        """
        self.root_dir = root_dir
        syn_names = os.listdir(os.path.join(root_dir, 'syn'))
        gt_names = os.listdir(os.path.join(root_dir, 'gt'))
        syn_names.sort()
        gt_names.sort()
        self.syn_names = syn_names
        self.gt_names = gt_names
        self.transform = transform
        self.sub = sub
        self.div = div
        self.mode = mode
        self.ignore_vox_thresh = ignore_vox_thresh
        self.max_dist = max_dist
        self.num_classes = num_classes
        # self.tsf = Transform(opt.min_size, opt.max_size, isTrain)
        self.data_shape = tif.imread(os.path.join(self.root_dir, 'syn', self.syn_names[0])).shape
        self.generate_in_fly = Config.generate_in_fly if self.mode == 'train' else False

    def __len__(self):
        return len(self.syn_names)
    
    def getsize(self):
        return self.data_shape[2], self.data_shape[0], self.data_shape[1]

    def __getitem__(self, idx):
        syn_dir = os.path.join(self.root_dir, 'syn', self.syn_names[idx])
        vol_syn = tif.imread(syn_dir).astype(np.uint8)
        if self.mode != 'inference':
            gt_dir = os.path.join(self.root_dir, 'gt', self.gt_names[idx])
            vol_gt = tif.imread(gt_dir)
        # vol_syn_new = np.zeros((128,128,128), dtype=np.uint8)
        # vol_gt_new = np.zeros((128,128,128), dtype=np.uint16)
        # vol_syn_new[:,:,0:vol_syn.shape[2]] = vol_syn
        # vol_gt_new[:,:,0:vol_gt.shape[2]] = vol_gt
        # vol_syn = vol_syn_new
        # vol_gt = vol_gt_new
        # if vol_syn.shape[2] < 64:    # if too few z slices
        #     vol_syn = np.pad(vol_syn, ((0, 0), (0, 0),(0, 64-vol_syn.shape[2])), 'constant',constant_values=0)
        #     vol_gt = np.pad(vol_gt, ((0, 0), (0, 0),(0, 64-vol_gt.shape[2])), 'constant',constant_values=0)

        if self.mode == 'train':
            # print(vol_syn.shape, vol_gt.shape)
            if random.choice([False, False]):
                # random crop
                len_y = random.choice(Config.crop)
                len_x = random.choice(Config.crop)
                len_z = random.choice(Config.crop)
                y_shift = 0 if len_y==128 else np.random.randint(vol_syn.shape[0]-len_y)
                x_shift = 0 if len_x==128 else np.random.randint(vol_syn.shape[1]-len_x)
                z_shift = 0 if len_z==128 else np.random.randint(vol_syn.shape[2]-len_z)
                # print(y_shift, x_shift, z_shift)
                vol_syn = vol_syn[y_shift:(y_shift+len_y), x_shift:(x_shift+len_x), z_shift:(z_shift+len_z)]
                vol_gt = vol_gt[y_shift:(y_shift+len_y), x_shift:(x_shift+len_x), z_shift:(z_shift+len_z)]
            # print(vol_syn.shape)
        if self.mode != 'inference':
            # load target
            if self.generate_in_fly:  # generate vector flow volume in a fly if data augmentation is needed
                vol_syn, vol_gt, flips = random_flip(vol_syn, vol_gt, x_random=True, y_random=True,z_random=False, return_param=True)
                img_ctrs = self.get_object_code(vol_gt, vox_threshold=self.ignore_vox_thresh)
                target = centroidnet.encode(img_ctrs, vol_gt, vol_gt.shape[0], vol_gt.shape[1], vol_gt.shape[2], self.max_dist, self.num_classes)
            else:   # save generated vector flow once and load next time
                img_ctrs = self.get_object_code(vol_gt, vox_threshold=self.ignore_vox_thresh)
                if self.mode=='train':
                    if os.path.exists(os.path.join(self.root_dir, 'centroidnet_target', self.syn_names[idx])):
                        target = tif.imread(os.path.join(self.root_dir, 'centroidnet_target', self.syn_names[idx]))
                    else:
                        target = centroidnet.encode(img_ctrs, vol_gt, vol_gt.shape[0], vol_gt.shape[1], vol_gt.shape[2], self.max_dist, self.num_classes)
                        if not os.path.exists(os.path.join(self.root_dir, 'centroidnet_target')):
                            os.makedirs(os.path.join(self.root_dir, 'centroidnet_target'))
                        tif.imsave(os.path.join(self.root_dir, 'centroidnet_target',self.syn_names[idx]), np.float32(target))        # if self.mode == 'train':        # if self.mode == 'train':
                else:
                        target = centroidnet.encode(img_ctrs, vol_gt, vol_gt.shape[0], vol_gt.shape[1], vol_gt.shape[2], self.max_dist, self.num_classes)
                        if not os.path.exists(os.path.join(self.root_dir, 'centroidnet_target')):
                            os.makedirs(os.path.join(self.root_dir, 'centroidnet_target'))
                        tif.imsave(os.path.join(self.root_dir, 'centroidnet_target',self.syn_names[idx]), np.float32(target))        # if self.mode == 'train':        # if self.mode == 'train':

        #     vol_syn, vol_gt, flips = random_flip(vol_syn, target, x_random=True, y_random=True,z_random=False, return_param=True)
        # save target for next time loading
        # if not os.path.exists(os.path.join(self.root_dir, 'centroidnet_target')):
        #     os.makedirs(os.path.join(self.root_dir, 'centroidnet_target'))
        # tif.imsave(os.path.join(self.root_dir, 'centroidnet_target',self.syn_names[idx]), np.float32(target))
        # apply gaussian filter
        # vol_syn = gaussian(vol_syn, preserve_range = True, sigma=1)
        # normalize vol_syn
        if self.mode == 'train' or self.mode == 'val':
            vol_syn = (vol_syn.astype(np.float32) - self.sub) / self.div
        vol_syn = vol_syn[np.newaxis,:]
        if self.mode != 'inference':
            target = target.astype(np.float32)
        if self.mode=='train':
            sample = {'img_id': self.syn_names[idx], 'image':vol_syn.copy(), 'target': target.copy()}
        elif self.mode == 'inference':
            sample = {'img_id': self.syn_names[idx], 'image':vol_syn.copy(), 'target': np.array([],dtype=np.float32), 'obj_code':np.array([],dtype=np.float32)}
        else:   # validation set and testing set has to have batch_size = 1
            sample = {'img_id': self.syn_names[idx], 'image':vol_syn.copy(), 'target': target.copy(),'obj_code': img_ctrs.copy()}   # 'obj_code' requires bach_size=1, because not every image has the same number of objects
        # if self.transform:
        #     sample = self.tsf(sample)
        
        return sample

    def get_object_code(self, vol_gt, vox_threshold):
        # object volume smaller than vol_threshold will be ignored
        nuclei_intensity = list(np.unique(vol_gt))
        nuclei_intensity.remove(0)
        object_code = []
        for idx in nuclei_intensity:
            # for ith nuclei
            # print(i, idx)
            h,w,z = vol_gt.shape
            (xs, ys, zs) = np.where(vol_gt == idx)
            y1, x1, z1, y2, x2, z2 = np.min(xs), np.min(ys), np.min(zs), np.max(xs), np.max(ys), np.max(zs)
            y, x, z = round((y2-y1)/2+y1), round((x2-x1)/2+x1), round((z2-z1)/2+z1)
            nucleus_vox = len(vol_gt[vol_gt == idx].flatten())
            if nucleus_vox>=vox_threshold:
                object_code.append([y, x, z, y1, y2, x1, x2, z1, z2, 0])

        return np.array(object_code, np.int16)
    
    def eval(self):
        self.train_mode = False


if __name__ == '__main__':
    data_dir = Config.data_dir
    # train_dir = os.path.join(data_dir, 'train_3Dtif')
    # val_dir = os.path.join(data_dir, 'val_3Dtif')
    test_dir = os.path.join(Config.test_dir, 'test_3D_tif')
    # training_set = centroidnet.NucleusDataset(train_dir, ignore_vox_thresh = Config.ignore_vox_thresh, max_dist = Config.max_dist, num_classes = Config.num_classes,
    #                                         crop = Config.crop, repeat = 1, sub = Config.sub, div = Config.div, transform=False, mode='train')
    # validation_set = centroidnet.NucleusDataset(val_dir, ignore_vox_thresh = Config.ignore_vox_thresh, max_dist = Config.max_dist, num_classes = Config.num_classes,
    #                                         crop = Config.crop, repeat = 1, sub = Config.sub, div = Config.div, transform=False, mode='val')
    test_set = centroidnet.NucleusDataset(test_dir, ignore_vox_thresh = Config.ignore_vox_thresh, max_dist = Config.max_dist, num_classes = Config.num_classes,
                                            crop = Config.crop, repeat = 1, sub = Config.sub, div = Config.div, transform=False, mode='test')
    # training_set_loader = torch.utils.data.DataLoader(training_set, batch_size=1, shuffle=False, num_workers=1)
    # validation_set_loader = torch.utils.data.DataLoader(validation_set, batch_size=1, shuffle=False, num_workers=4)
    test_set_loader = torch.utils.data.DataLoader(test_set, batch_size=1, shuffle=False, num_workers=4)
    # print(training_set.getsize())
    # for i, sample in enumerate(training_set_loader):
    #     print(sample['img_id'])
    # for i, sample in enumerate(validation_set_loader):
    #     print(sample['obj_code'].shape)
    for i, sample in enumerate(test_set_loader):
        print(sample['img_id'])

    # for i in range(0, len(validation_set)):
    #     print(training_set[i]['image'].shape, training_set[i]['target'].shape, training_set[i]['obj_code'].shape)
