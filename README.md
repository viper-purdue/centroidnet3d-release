<!---
#Readme for 3D CentroidNet Package 12/30/2022
#Copyright 2022 - The Board of Trustees of Purdue University - All rights reserved
#This is the training, testing, and inference code for 3D CentroidNet.

#This source code is to be used for academic research purposes only, and no commercial use is allowed.
-->

*****
# Term of Use and License
Version 1.1
December 30, 2022


This work was partially supported by a George M. O’Brien Award from the National Institutes of Health 
under grant NIH/NIDDK P30 DK079312 and the endowment of the Charles William Harrison Distinguished 
Professorship at Purdue University.

Copyright and Intellectual Property

The software/code and data are distributed under Creative Commons license
Attribution-NonCommercial-ShareAlike - CC BY-NC-SA

You are free to:
* Share — copy and redistribute the material in any medium or format
* Adapt — remix, transform, and build upon the material
* The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:
* Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use. (See below for paper citation)
* NonCommercial — You may not use the material for commercial purposes.
* ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.
* No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

For more information see:
https://creativecommons.org/licenses/by-nc-sa/4.0/


The data is distributed under Creative Commons license
Attribution-NonCommercial-NoDerivs - CC BY-NC-ND

You are free to:
* Share — copy and redistribute the material in any medium or format
* The licensor cannot revoke these freedoms as long as you follow the license terms.

Under the following terms:
* Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
* NonCommercial — You may not use the material for commercial purposes.
* NoDerivatives — If you remix, transform, or build upon the material, you may not distribute the modified material.
* No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.

For More Information see:
https://creativecommons.org/licenses/by-nc-nd/4.0/



Attribution
In any publications you produce based on using our software/code or data we ask that you cite the following paper:
```BibTeX
@ARTICLE{CentroidNet3D,
  author={Liming Wu, Alain Chen, Paul Salama, Kenneth W. Dunn, and Edward J. Delp},
  title={3D CentroidNet: Nuclei Centroid Detection With Vector Flow Voting},
  Journal={Proceedings of the IEEE International Conference on Image Processing (ICIP)},
  year={2022},
  month={October},
  url={https://doi.org/10.1101/2022.07.21.500996}
}
```

Privacy Statement
We are committed to the protection of personal privacy and have adopted a policy to  protect information about individuals. 
When using our software/source code we do not collect any information about you or your use of the code.

How to Contact Us

The source code/software and data is distributed "as is." 
We do not accept any responsibility for the operation of the software.
We can only provided limited help in running our code. 
Note: the software/code developed under Linux. 
We do not do any work with the Windows OS and cannot provide much help in running our software/code on the Windows OS

If you have any questions contact: imart@ecn.purdue.edu

*****

# How to use this repository

We provide to datasets for demo, **D1** and **D2**
1. download the trained demo models: `wget https://skynet.ecn.purdue.edu/~micro/centroidNet3D_release/checkpoints.zip --no-check-certificate`
2. unzip the trained demo models: `unzip checkpoints.zip`
3. download the trained demo models: `wget https://skynet.ecn.purdue.edu/~micro/centroidNet3D_release/data.zip --no-check-certificate`
4. unzip the trained demo models: `unzip data.zip`

Before testing and training, need to modify the configurations in `config.py`

# 3D CentroidNet Testing
1. modify training settings in `config.py`
2. `python test.py`
3. The results will be saved in folder `data`

# 3D CentroidNet training
1. modify training settings in `config.py`
2. `python train.py`
3. The trained weights will be saved in folder `checkpoints`



# References

Dijkstra, K., van de Loosdrecht, J., Schomaker, L.R.B., Wiering, M.A. (2019). CentroidNet: A Deep Neural Network for Joint Object Localization and Counting. In: , et al. Machine Learning and Knowledge Discovery in Databases. ECML PKDD 2018. Lecture Notes in Computer Science(), vol 11053. Springer, Cham. https://doi.org/10.1007/978-3-030-10997-4_36


