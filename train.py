import torch.utils.data
import torch.optim as optim
from tqdm import tqdm
import copy
from config import Config
from centroidnet import *
import skimage.external.tifffile as tif
import torch.nn as nn
import os
import math
from utils.vis_tool import Visualizer
import utils.array_tool as at
from utils.watershed3d import watershed3d
from utils.CC2 import CC2
import pandas as pd
from skimage.morphology import erosion, ball
from utils.eval_f1 import eval_count, avg_dice

def create_data_loader(data_dir,ignore_vox_thresh, num_classes, crop, max_dist, repeat, sub, div):
    train_dir = os.path.join(data_dir, 'train_3Dtif')
    val_dir = os.path.join(data_dir, 'val_3Dtif')
    test_dir = os.path.join(Config.test_dir, 'test_3D_tif')
    training_set = centroidnet.NucleusDataset(train_dir, ignore_vox_thresh = ignore_vox_thresh, max_dist = max_dist, num_classes = num_classes,
                                                crop = crop, repeat = repeat, sub = sub, div = div, transform=False, mode='train')
    validation_set = centroidnet.NucleusDataset(val_dir, ignore_vox_thresh = ignore_vox_thresh, max_dist = max_dist, num_classes = num_classes, 
                                            crop = crop, repeat = repeat, sub = sub, div = div, transform=False, mode='val')
    testing_set = centroidnet.NucleusDataset(test_dir, ignore_vox_thresh = ignore_vox_thresh, max_dist = max_dist, num_classes = num_classes,
                                                crop = crop, repeat = repeat, sub = sub, div = div, transform=False, mode='test')
    
    return training_set, validation_set, testing_set


def create_centroidnet(num_channels, num_classes):
    model = centroidnet.CentroidNet(num_classes, num_channels)
    return model


def create_centroidnet_loss():
    if Config.loss == 'FocalTverskyLoss':
        loss = [centroidnet.CentroidLoss(), centroidnet.TverskyLoss(alpha=0.3, beta=0.7), centroidnet.FocalLoss()]
    else:
        loss = [centroidnet.CentroidLoss(), centroidnet.DiceLoss(), nn.BCELoss()]
    return loss


def validate(validation_loss, epoch, validation_set_loader, model, loss, validation_interval=10):
    if epoch % validation_interval == 0:
        with torch.no_grad():
            # Validate using validation data loader
            model.eval() # put in evaluation mode
            validation_loss = 0
            idx = 0
            for sample in validation_set_loader:
                inputs = sample['image']
                targets = sample['target']
                inputs = inputs.to(Config.dev)
                targets = targets.to(Config.dev)
                outputs = model(inputs)
                ls1 = 10*loss[0](outputs[:,0:3,:,:,:], targets[:,0:3,:,:,:])   # MSE Loss
                ls3 = 10*loss[2](outputs[:,3:5,:,:,:], targets[:,3:5,:,:,:])   # BCE Loss
                ls2 = loss[1](outputs[:,3:5,:,:,:], targets[:,3:5,:,:,:])   # Dice Loss
                ls = ls1 + ls3 + ls2
                validation_loss += ls.item()
                idx += 1
            model.train() # put back in training mode

            return validation_loss / idx
    else:
        return validation_loss


def save_model(filename, model):
    print(f"Save snapshot to: {os.path.abspath(filename)}")
    with open(filename, "wb") as f:
        torch.save(model.state_dict(), f)


def load_model(filename, model):
    print(f"Load snapshot from: {os.path.abspath(filename)}")
    with open(filename, "rb") as f:
        state_dict = torch.load(f)
    model.load_state_dict(state_dict)
    return model


def train(training_set, validation_set, test_set, model, loss, epochs, batch_size, learn_rate, validation_interval, vis):
    print(f"Training {len(training_set)} images for {epochs} epochs with a batch size of {batch_size}.\n"
          f"Validate {len(validation_set)} images each {validation_interval} epochs and learning rate {learn_rate}.\n")

    model = nn.DataParallel(model)

    if Config.pre_train == True:
        model = load_model(Config.pre_train_path, model)

    best_model = copy.deepcopy(model)

    model.to(Config.dev)
    
    loss[0].to(Config.dev)
    loss[1].to(Config.dev)
    loss[2].to(Config.dev)

    optimizer = optim.Adam(model.parameters(), lr=learn_rate)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=100, gamma=0.1)

    training_set_loader = torch.utils.data.DataLoader(training_set, batch_size=batch_size, shuffle=True, num_workers=1, drop_last=True)
    validation_set_loader = torch.utils.data.DataLoader(validation_set, batch_size=1, shuffle=True, num_workers=1, drop_last=True)

    if len(training_set_loader) == 0:
        raise Exception("The training dataset does no contain any samples. Is the minibatch larger than the amount of samples?")
    if len(training_set_loader) == 0:
        raise Exception("The validation dataset does no contain any samples. Is the minibatch larger than the amount of samples?")

    bar = tqdm(range(1, epochs))
    validation_loss = 9999
    best_loss = 9999
    best_f1=0
    best_acc = 0
    for epoch in bar:
        training_loss = 0
        train_ls1 = 0
        train_ls2 = 0
        idx = 0
        # Train one minibatch
        for i, sample in enumerate(training_set_loader):
            inputs = sample['image']
            targets = sample['target']
            inputs = inputs.to(Config.dev)
            targets = targets.to(Config.dev)
            optimizer.zero_grad()
            outputs = model(inputs)
            ls1 = 10*loss[0](outputs[:,0:3,:,:,:], targets[:,0:3,:,:,:])   # MSE Loss
            ls3 = 10*loss[2](outputs[:,3:5,:,:,:], targets[:,3:5,:,:,:])   # BCE Loss

            ls2 = loss[1](outputs[:,3:5,:,:,:], targets[:,3:5,:,:,:])   # Dice Loss
            ls = ls1 + ls3 + ls2
            ls.backward()
            optimizer.step()
            training_loss += ls.item()
            train_ls1 += ls1.item()
            train_ls2 += ls2.item()
            idx += 1
            if i % 5 == 0:
                ori_vol_ = at.tonumpy(inputs[0])*Config.div + Config.sub
                ori_slice = ori_vol_[0,:,:,30]  # choose the 30th slice
                vec_x = (at.tonumpy(targets[0,0,:,:,30])+1)/2
                vec_y = (at.tonumpy(targets[0,1,:,:,30])+1)/2
                vec_z = (at.tonumpy(targets[0,2,:,:,30])+1)/2
                mask = at.tonumpy(targets[0,4,:,:,30])
                vis.img('input image', ori_slice)
                vis.img('gt vector_x', vec_x)
                vis.img('gt vector_y', vec_y)
                vis.img('gt vector_z', vec_z)
                vis.img('gt mask', (mask >= 0.5).astype(np.float32))

                vec_x = np.clip((at.tonumpy(outputs[0,0,:,:,30])+1)/2, 0, 1)
                vec_y = np.clip((at.tonumpy(outputs[0,1,:,:,30])+1)/2, 0, 1)
                vec_z = np.clip((at.tonumpy(outputs[0,2,:,:,30])+1)/2, 0, 1)
                mask = at.tonumpy(outputs[0,4,:,:,30])
                vis.img('pred vector_x train', vec_x)
                vis.img('pred vector_y train', vec_y)
                vis.img('pred vector_z train', vec_z)
                vis.img('pred mask train', (mask >= 0.5).astype(np.float32))
 
        scheduler.step(epoch)
        # Validate and save
        validation_loss = validate(validation_loss, epoch, validation_set_loader, model, loss, validation_interval)
        # Update progress bar
        bar.set_description("Epoch {}/{} Loss(T): {:5f} and Loss(V): {:.5f}".format(epoch, epochs, training_loss / idx, validation_loss))
        bar.refresh()
        # plot loss on visdom
        if training_loss<100 and validation_loss<100:
            vis.plot_many({'train loss':training_loss/idx, 'validation loss':validation_loss})
        
        if validation_loss < best_loss:
            print(f"Update model with loss {validation_loss}")
            best_loss = validation_loss
            best_model.load_state_dict(model.state_dict())
            save_model(os.path.join("checkpoints", "CentroidNet_"+Config.data_name+".pth"), best_model)
    save_model(os.path.join("checkpoints", "CentroidNet_"+Config.data_name+"_last_epoch.pth"), model)
    
    return best_model


def predict(data_set, model, loss, max_dist, binning, nm_size, centroid_threshold):
    print(f"Predicting {len(data_set)} files with loss {type(loss)}")
    with torch.no_grad():
        data_set.eval()
        model.eval()
        model.to(Config.dev)
        loss_value = 0
        idx = 0
        # inference only support batch size = 1
        block=64
        size_z, size_h, size_w = data_set.getsize()
        w_p = int(math.ceil(size_w/float(block/2))*(block/2))
        h_p = int(math.ceil(size_h/float(block/2))*(block/2))

        padh = h_p-size_h
        padw = w_p-size_w
        print("x: " + str(h_p) + ", y: " + str(w_p) )
        set_loader = torch.utils.data.DataLoader(data_set, batch_size=1, shuffle=False, num_workers=1, drop_last=False)
        result_images = []
        result_centroids = []
        obj_codes = []
        for sample in tqdm(set_loader):
            inputs = sample['image']
            obj_codes.append(sample['obj_code'])
            targets = sample['target']
            # inputs = inputs.to(Config.dev)
            input_numpy = np.transpose(np.squeeze(inputs.data[0].cpu().numpy()), (2,0,1))
            input_padded = np.pad(input_numpy, ((0, 0), (0, padh), (0, padw)), 'reflect')
            input_inference = np.pad(input_padded, ((0,0), (int(block/4), int(block/4)), (int(block/4), int(block/4))), 'reflect')
            
            inputs_sub = np.zeros([1,1,size_z,block,block])
            output = np.zeros([5,size_z,h_p,w_p])  # 5 channels
            for kk in range(0,int(w_p/(block/2))):
                for jj in range(0,int(h_p/(block/2))):
                    print(int(block/2)*jj, int(block/2)*(jj+2))
                    inputs_sub[0,0,:,:,:] = input_inference[:,int(block/2)*jj:int(block/2)*(jj+2),int(block/2)*kk:int(block/2)*(kk+2)]
                    # normalize each slice
                    for z in range(0, inputs_sub.shape[2]):
                        inputs_sub[:,:,z,:,:] = (inputs_sub[:,:,z,:,:]-np.min(inputs_sub[:,:,z,:,:]))/np.max(inputs_sub[:,:,z,:,:])*255
                    inputs_sub = (inputs_sub - Config.sub) / Config.div

                    inputs_sub = np.transpose(inputs_sub, (0,1,3,4,2))
                    inputs_sub = torch.from_numpy(inputs_sub).float()
                    inputs_sub = inputs_sub.to(Config.dev)
                    output_sub = model(inputs_sub)
                    output_sub = np.squeeze(output_sub.data.cpu().numpy())
                    output_sub = np.transpose(output_sub, (0,3,1,2))
                    output[:,:,int(block/2)*jj:int(block/2)*(jj+1),int(block/2)*kk:int(block/2)*(kk+1)] = output_sub[:,:,int(block/4):int(3*block/4),int(block/4):int(3*block/4)]
                    print('output:', (int(block/2)*jj, int(block/2)*(jj+1)), (int(block/2)*jj, int(block/2)*(jj+1)))
                    print('output_sub:', (int(block/4),int(3*block/4)), (int(block/4), int(3*block/4)))
                    inputs_sub = inputs_sub.data.cpu().numpy()
                    inputs_sub = np.transpose(inputs_sub, (0,1,4,2,3))

            output = np.transpose(output[:,0:size_z,:,:].astype(np.float32), (0,2,3,1))
            decoded = [centroidnet.decode(output, max_dist, binning, nm_size, centroid_threshold)]

            # Add all numpy arrays to a list
            result_images.extend([{"inputs": i.cpu().numpy(),
                                   "targets": t.cpu().numpy(),
                                   "vectors": d[0],
                                   "votes": d[1],
                                   "class_ids": d[2],
                                   "class_probs": d[3],
                                   "centroids": d[4]} for i, t, d in zip(inputs, targets, decoded)])

            # Add image_id to centroid locations and add to list
            result_centroids.extend([np.stack(ctr for ctr in d[5]) for d in decoded])
            idx = idx + 1
    return result_images, result_centroids, obj_codes

def output(folder, result_images, result_centroids, obj_codes):
    os.makedirs(folder, exist_ok=True)
    print(f"Created output folder {os.path.abspath(folder)}")
    
    for i, sample in enumerate(result_images):
        for name, arr in sample.items():
            tif.imsave(os.path.join(folder, f"{str(i+1).zfill(2)}_{name}.tif"), np.transpose(arr, (3,0,1,2)), imagej=True)

    lines = ["image_nr centroid_y centroid_x centroid_z class_id probability \r\n"]
    with open(os.path.join(folder, "validation.txt"), "w") as f:
        for i, image in enumerate(result_centroids):
            for line in image:
                line_str = [str(i+1), *[str(elm) for elm in line]]
                lines.append(" ".join(line_str) + "\r\n")
        f.writelines(lines)

    # Instance segmentation using marker-controlled 3D watershed
    for i, sample in enumerate(result_images):
        instance_seg_vol = watershed3d(sample['class_ids'][0], sample['centroids'][0])
        tif.imsave(os.path.join(folder, f"{str(i+1).zfill(2)}_instance_seg.tif"), np.transpose(instance_seg_vol, (2,0,1)))
        # Color code the segmentation results
        cc_instance_seg_vol = CC2(instance_seg_vol)
        tif.imsave(os.path.join(folder, f"{str(i+1).zfill(2)}_instance_seg_CC2.tif"), np.transpose(cc_instance_seg_vol, (2,0,1,3)))


    # visualize centers & evaluate f1 score
    center_size = 1 if Config.data_name=='wsm' else 2
    eval_results = []
    data_names = []
    count = []
    for i, (result_image, result_centroid) in enumerate(zip(result_images, result_centroids)):
        vis_volume = np.uint8(result_image['targets'][-1,:])
        coords = np.uint16(result_centroid[:,0:3])
        vis_volume = np.uint8(vis_volume[:,:,:,np.newaxis])
        vis_volume = np.repeat(vis_volume, 3, axis=3)
        vis_volume[vis_volume>0] = 64
        gt_centers = obj_codes[i].numpy()[0,:,0:3]
        print('number of predicted centers: '+str(len(coords)))
        count.append(len(coords))
        for pred_center in coords:
            vis_volume[(pred_center[0]-center_size):(pred_center[0]+center_size+1),(pred_center[1]-center_size):(pred_center[1]+center_size+1),(pred_center[2]-center_size):(pred_center[2]+center_size+1),0]=255
        
        for gt_center in gt_centers:
            vis_volume[(gt_center[0]-center_size):(gt_center[0]+center_size+1),(gt_center[1]-center_size):(gt_center[1]+center_size+1),(gt_center[2]-center_size):(gt_center[2]+center_size+1),1]=255
        tif.imsave(os.path.join(folder, f"{str(i+1).zfill(2)}_vis_center.tif"), np.transpose(vis_volume, (2,0,1,3)))
        precision, recall, f1 = eval_count(gt_centers, coords, dis_threshold=Config.dist_threshold)
        eval_results.append([precision, recall, f1])
        data_names.append('vol'+str(i+1).zfill(3))
    eval_results = np.array(eval_results)
    df = pd.DataFrame(zip(data_names, eval_results[:,0], eval_results[:,1], eval_results[:,2]), columns=['img_id','precision','recall','f1'])
    df.to_csv(os.path.join(folder, 'P_R_F1.csv'), index=False)
    count_df = pd.DataFrame(zip(data_names, np.array(count)), columns=['img_id', 'count'])
    count_df.to_csv(os.path.join(folder, 'count.csv'), index=False)
    print('precision:',np.round(np.average(df['precision']),4), ', recall:', np.round(np.average(df['recall']),4), ',f1:',np.round(np.average(df['f1']),4))

    # voxel based dice score
    acc = []
    for i, sample in enumerate(result_images):
        # eroded = dilation(np.squeeze(sample['class_ids']), ball(1))
        eroded = np.squeeze(sample['class_ids'])
        TypeI, typeII, accuracy = avg_dice(eroded, np.uint8(sample['targets'][4]))
        acc.append([TypeI, typeII, accuracy])
    acc = np.array(acc)
    df_dice = pd.DataFrame(zip(data_names, acc[:,0], acc[:,1], acc[:,2]), columns=['img_id','TypeI','typeII','accuracy'])
    df_dice.to_csv(os.path.join(folder, 'dice.csv'), index=False)
    print('TypeI: ', np.round(np.average(df_dice['TypeI']),4), ', typeII:', np.round(np.average(df_dice['typeII']),4), ',accuracy:',np.round(np.average(df_dice['accuracy']),4))

    return np.round(np.average(df['f1']),4), np.round(np.average(df_dice['accuracy']),4)

def main():
    # Perform retraining.
    do_train = True
   # Perform loading of the model.
    do_load = False
    # Perform final prediction and export data.
    do_predict = False

    # Start script
    assert (do_train or do_load), "Enable do_train and/or do_load"

    # Load datasets
    training_set, validation_set, test_set = create_data_loader(Config.data_dir, ignore_vox_thresh = Config.ignore_vox_thresh, 
                    num_classes = Config.num_classes, crop=Config.crop,max_dist=Config.max_dist, repeat=1, sub=Config.sub, div=Config.div)

    assert training_set.num_classes == Config.num_classes, f"Number of classes on config.py is incorrect. Should be {training_set.num_classes}"

    # Create loss function
    loss = create_centroidnet_loss()
    model = None

    # create visualization tool
    vis = Visualizer(env=Config.env) 

    # Train network
    if do_train:
        # Create network and load snapshots
        model = create_centroidnet(num_channels=Config.num_channels, num_classes=Config.num_classes)
        model = train(training_set, validation_set, test_set, model, loss, epochs=Config.epochs, batch_size=Config.batch_size, learn_rate=Config.learn_rate, validation_interval=Config.validation_interval, vis=vis)
        # save_model(os.path.join("checkpoints", "CentroidNet_"+Config.data_name+".pth"), model)

    # Load model
    if do_load:
        model = create_centroidnet(num_channels=1, num_classes=training_set.num_classes)
        model = nn.DataParallel(model)
        model = load_model(os.path.join("checkpoints", "CentroidNet_"+Config.data_name+".pth"), model)

    # Predict
    if do_predict:
        result_images, result_centroids, obj_codes = predict(validation_set, model, loss, max_dist=Config.max_dist, binning=Config.binning, nm_size=Config.nm_size, centroid_threshold=Config.centroid_threshold)
        output(os.path.join("data", "validation_result_"+Config.data_name), result_images, result_centroids, obj_codes)


if __name__ == '__main__':
    main()
